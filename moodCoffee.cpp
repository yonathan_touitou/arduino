#include "moodCoffee.h"
#include "moodCommon.h"

void moodCofee() {
	for (int r = 3; r < 8; r++) {
		SetRow(GREEN, r, arr_coffeeMug[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 3; r++) {
		SetRow(RED, r, arr_coffeeSteam0[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 3; r++) {
		SetRow(RED, r, arr_coffeeSteam1[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 3; r++) {
		SetRow(RED, r, arr_coffeeSteam2[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 3; r++) {
		SetRow(RED, r, arr_coffeeSteam3[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 3; r++) {
		SetRow(RED, r, 0x00);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
}


void moodCoffeeSetup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodCoffeeLoop() {
	while (moodShouldPlay) {
		moodCofee();
	}
	ClearMatrix();
}

void runMoodCoffee() {
	moodCoffeeSetup();
	moodCoffeeLoop();
}