static bool moodShouldPlay = false;

static bool checkIfMoodSouldEnd() {
	if (Serial.available()) {
		String data = Serial.readString();
		if (data[0] == 'b') {
			moodShouldPlay = false;
			return true;
		}
	}
	return false;
}
