#include "LedControl3.h"

void snakeLoop();
void addHead(int x, int y);
void snakeInit();
void setDirection();
void moveSnake(int direction);
void newApple();
boolean check(int x, int y);
void screenUpdate();
void screenDisplay();
void clrscr();
void snakeSetup();
void snakeLoop();