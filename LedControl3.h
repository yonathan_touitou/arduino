#include "LedControlManager.h"
#include "ReadIrManager.h"

// Games selection codes
const char SELECTION_SNAKE     = '1';
const char SELECTION_PONG      = '2';
const char SELECTION_DRAW      = '3';
const char SELECTION_TICTACTOE = '4';

const char SELECTION_MOODCOFFEE = 'A';
const char SELECTION_MOODLOVE   = 'B';
const char SELECTION_MOODSMILY  = 'C';
const char SELECTION_MOODSTARS  = 'D';
const char SELECTION_MOODPARTY1 = 'E';
const char SELECTION_MOODPARTY2 = 'F';
const char SELECTION_MOODPARTY3 = 'G';

const char SELECTION_TESTS = '0';

// Games running functions declerations
void runSnake();
void runPong();
void runDraw();
void runTicTacToe();

// Moods functions declerations
void runMoodCoffee();
void runMoodLove();
void runMoodSmily();
void runMoodStars();
void runMoodParty1();
void runMoodParty2();
void runMoodParty3();

// Tests functions declerations
void runTests();