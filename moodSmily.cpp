#include "moodSmily.h"
#include "moodCommon.h"

void moodSmily() {
	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, smile_bmp[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
}

void moodSmilySetup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodSmilyLoop() {
	while (moodShouldPlay) {
		moodSmily();
	}
	ClearMatrix();
}



void runMoodSmily() {
	moodSmilySetup();
	moodSmilyLoop();
}