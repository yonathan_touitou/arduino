#include "Snake.h"

bool shouldEndGame = false;

// Screen
byte led[8];

// game variables
typedef struct Link {
	int x;
	int y;
	struct Link * next;
} Link;

Link* pHead = NULL;
Link* pTail = NULL;

int curDirection = 4;
int newDirection = 4;
int appleX = 5;
int appleY = 5;

byte sad1[] = {
	B00000000,
	B01000100,
	B00010000,
	B00010000,
	B00000000,
	B00111000,
	B01000100,
	B00000000
};

unsigned long oldTimer, curTimer;

boolean dead = 0;

void runSnake() {
	snakeSetup();
	snakeLoop();
}


void snakeSetup() {
	Serial.begin(9600);
	ClearMatrix();
}

void snakeLoop() {
	while (true) {
		shouldEndGame = false;
		dead = false;
		snakeInit();
		screenUpdate();
		oldTimer = millis();
		curTimer = millis();

		while (!dead) {
			curTimer = millis();

			setDirection();
			if (shouldEndGame) {
				clrscr();
				return;
			}

			if (curTimer - oldTimer >= 160) {
				curDirection = newDirection;
				moveSnake(curDirection);
				screenUpdate();
				oldTimer = millis();
			}

			// update screen
			screenDisplay();
		}

		Serial.write("l"); // Inform the application that the player lost

		clrscr();
		for (int r = 0; r < 8; r++) {
			SetRow(GREEN, r, sad1[r]); // 2 colors
		}
		delay(1500);
		clrscr();
	}
}

void addHead(int x, int y) {
	Link *temp;
	temp = (Link*)malloc(sizeof(Link)); //TODO: need to FREE allocations

										// create new head
	temp->x = x;
	temp->y = y;
	temp->next = NULL;

	if (pHead != NULL)
		pHead->next = temp;

	// point to new head
	pHead = temp;
}


void snakeInit() {
	int x = 3;
	int y = 3;
	for (int i = 0; i < 2; i++, x++) {
		addHead(x, y);
		if (i == 0)
			pTail = pHead;
	}
}

void setDirection() {
	char direction = '0';
	if (Serial.available()) {
		direction = Serial.read();

		if (direction == 'b') {
			shouldEndGame = true;
			return;
		}
	}
	char IRinput = getIrInput();
	if (IRinput != NO_IR_INPUT) {
		direction = IRinput;
	}

	if (direction != '0') {
		if (direction == 'w') {
			if (curDirection != 2)
				newDirection = 1;
		}
		if (direction == 's') {
			if (curDirection != 1)
				newDirection = 2;
		}
		if (direction == 'a') {
			if (curDirection != 4)
				newDirection = 3;
		}
		if (direction == 'd') {
			if (curDirection != 3)
				newDirection = 4;
		}
	}
}

void moveSnake(int direction) {
	int newX = pHead->x;
	int newY = pHead->y;
	if (direction == 1)
		newY--;
	if (direction == 2)
		newY++;
	if (direction == 3)
		newX--;
	if (direction == 4)
		newX++;

	if (newX > 8)
		newX = 1;
	if (newX < 1)
		newX = 8;
	if (newY > 8)
		newY = 1;
	if (newY < 1)
		newY = 8;

	dead |= check(newX, newY);

	if (!dead) {
		if (newX == appleX && newY == appleY) {
			Serial.write("1"); // Inform the application that an apple was eaten
			addHead(newX, newY);
			newApple();
		}
		else {
			Link *temp = pTail;

			// point to new tail
			pTail = pTail->next;

			// new head
			pHead->next = temp;
			pHead = temp;

			pHead->x = newX;
			pHead->y = newY;
			pHead->next = NULL;
		}
	}
}

void newApple() {
	boolean check = 0;
	Link * ptr = pTail;
	do {
		check = 0;
		appleX = random(7) + 1;
		appleY = random(7) + 1;
		while (ptr != NULL) {
			if (appleX == (ptr->x) && appleY == (ptr->y)) {
				check = 1;
				break;
			}
			ptr = ptr->next;
		}
	} while (check == 1);
}

boolean check(int x, int y) {
	Link *ptr;
	ptr = pTail;
	while (ptr != NULL)
	{
		if (x == ptr->x && y == ptr->y)
			return 1;
		ptr = ptr->next;
	}

	return 0;
}

void screenUpdate() {
	Link * ptr;
	ptr = pTail;

	clrscr();

	for (int i = 0; i < 8; i++) {
		led[i] = B00000000;
	}

	while (ptr != NULL) {
		led[ptr->y - 1] = led[ptr->y - 1] | (1 << (8 - ptr->x));
		ptr = ptr->next;
	}

	led[appleY - 1] = led[appleY - 1] | (1 << (8 - appleX));
}

void screenDisplay() {
	for (int i = 0; i < 8; i++) {
		if (i == appleY - 1) {
			byte tmp = (1 << (8 - appleX));
			SetRow(GREEN, i, (led[i] & ~tmp));
		}
		else {
			SetRow(GREEN, i, (led[i]));
		}
	}
	SetRow(RED, appleY - 1, (1 << (8 - appleX)));
}

void clrscr() {
	ClearMatrix();
}