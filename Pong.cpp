#include "Pong.h"

//#define DEBUG 1

bool shouldEndGame2 = false;

byte sad[] = {
	B00000000,
	B01000100,
	B00010000,
	B00010000,
	B00000000,
	B00111000,
	B01000100,
	B00000000
};

//byte smile[] = {
//	B00000000,
//	B01000100,
//	B00010000,
//	B00010000,
//	B00010000,
//	B01000100,
//	B00111000,
//	B00000000
//};


Timer timer;

byte direction; // Wind rose, 0 is north
int xball;
int yball;
int yball_prev;
byte xpad;
int ball_timer;

void setSprite(byte *sprite) {
	for (int r = 0; r < 8; r++) {
		//lc2.setRow(0, r, sprite[r]);
		SetRow(GREEN, r, sprite[r]); // 2 colors
	}
}

void newGame() {
	shouldEndGame2 = false;
	//lc2.clearDisplay(0);
	ClearMatrix(); // 2 colors
	
	// initial position
	xball = random(1, 7);
	yball = 1;
	direction = random(3, 6); // Go south
	//for (int r = 0; r < 8; r++) {
	//	for (int c = 0; c < 8; c++) {
	//		//lc2.setLed(0, r, c, HIGH);
	//		SetLed(GREEN, r, c, HIGH); // 2 colors
	//		delay(NEW_GAME_ANIMATION_SPEED);
	//	}
	//}
	/*setSprite(smile);
	delay(1500);*/
	//lc2.clearDisplay(0);
	ClearMatrix(); // 2 colors
	
}

void setPad() {
	char direction;
	if (Serial.available()) {
		direction = Serial.read();
		if (direction == 'b') {
			shouldEndGame2 = true;
			return;
		}
	}
	char IRinput = getIrInput();
	if (IRinput == RIGHT || IRinput == LEFT) {
		direction = IRinput;
	}
	if (direction == 'a') {
		if (xpad < 5) {
			xpad++;
		}
	}
	else if (direction == 'd') {
		if (xpad > 0) {
			xpad--;
		}
	}
	delay(100);
}

void debug(const char* desc) {
#ifdef DEBUG
	Serial.print(desc);
	Serial.print(" XY: ");
	Serial.print(xball);
	Serial.print(", ");
	Serial.print(yball);
	Serial.print(" XPAD: ");
	Serial.print(xpad);
	Serial.print(" DIR: ");
	Serial.println(direction);
#endif
}

int checkBounce() {
	if (!xball || !yball || xball == 7 || yball == 6) {
		int bounce = (yball == 0 || yball == 6) ? BOUNCE_HORIZONTAL : BOUNCE_VERTICAL;
#ifdef DEBUG
		debug(bounce == BOUNCE_HORIZONTAL ? "HORIZONTAL" : "VERTICAL");
#endif
		return bounce;
	}
	return 0;
}

int getHit() {
	if (yball != 6 || xball < xpad || xball > xpad + PADSIZE) {
		return HIT_NONE;
	}
	Serial.write("1"); // Inform the application that the ball hit the pad
	if (xball == xpad + PADSIZE / 2) {
		return HIT_CENTER;
	}
	return xball < xpad + PADSIZE / 2 ? HIT_LEFT : HIT_RIGHT;
}

bool checkLoose() {
	return yball == 6 && getHit() == HIT_NONE;
}

void moveBall() {
	debug("MOVE");
	int bounce = checkBounce();
	if (bounce) {
		switch (direction) {
		case 0:
			direction = 4;
			break;
		case 1:
			direction = (bounce == BOUNCE_VERTICAL) ? 7 : 3;
			break;
		case 2:
			direction = 6;
			break;
		case 6:
			direction = 2;
			break;
		case 7:
			direction = (bounce == BOUNCE_VERTICAL) ? 1 : 5;
			break;
		case 5:
			direction = (bounce == BOUNCE_VERTICAL) ? 3 : 7;
			break;
		case 3:
			direction = (bounce == BOUNCE_VERTICAL) ? 5 : 1;
			break;
		case 4:
			direction = 0;
			break;
		}
		debug("->");
	}

	// Check hit: modify direction is left or right
	switch (getHit()) {
	case HIT_LEFT:
		if (direction == 0) {
			direction = 7;
		}
		else if (direction == 1) {
			direction = 0;
		}
		break;
	case HIT_RIGHT:
		if (direction == 0) {
			direction = 1;
		}
		else if (direction == 7) {
			direction = 0;
		}
		break;
	}

	// Check orthogonal directions and borders ...
	if ((direction == 0 && xball == 0) || (direction == 4 && xball == 7)) {
		direction++;
	}
	if (direction == 0 && xball == 7) {
		direction = 7;
	}
	if (direction == 4 && xball == 0) {
		direction = 3;
	}
	if (direction == 2 && yball == 0) {
		direction = 3;
	}
	if (direction == 2 && yball == 6) {
		direction = 1;
	}
	if (direction == 6 && yball == 0) {
		direction = 5;
	}
	if (direction == 6 && yball == 6) {
		direction = 7;
	}

	// "Corner" case
	if (xball == 0 && yball == 0) {
		direction = 3;
	}
	if (xball == 0 && yball == 6) {
		direction = 1;
	}
	if (xball == 7 && yball == 6) {
		direction = 7;
	}
	if (xball == 7 && yball == 0) {
		direction = 5;
	}

	yball_prev = yball;
	if (2 < direction && direction < 6) {
		yball++;
	}
	else if (direction != 6 && direction != 2) {
		yball--;
	}
	if (0 < direction && direction < 4) {
		xball++;
	}
	else if (direction != 0 && direction != 4) {
		xball--;
	}
	xball = max(0, min(7, xball));
	yball = max(0, min(6, yball));
	debug("AFTER MOVE");
}

void gameOver() {
	Serial.write("l"); // Inform the application that the player lost
	ClearMatrix(); // 2 colors
	setSprite(sad);
	delay(1500);
	//lc2.clearDisplay(0);
	ClearMatrix(); // 2 colors
}

void drawGame() {
	if (yball_prev != yball) {
		//lc2.setRow(0, yball_prev, 0);
		SetRow(RED, yball_prev, 0); // 2 colors
	}
	//lc2.setRow(0, yball, byte(1 << (xball)));
	SetRow(RED, yball, byte(1 << (xball))); // 2 colors
	byte padmap = byte(0xFF >> (8 - PADSIZE) << xpad);
#ifdef DEBUG
	//Serial.println(padmap, BIN);
#endif
	//lc2.setRow(0, 7, padmap);
	SetRow(GREEN, 7, padmap); // 2 colors
}

void pongSetup() {
	timer = Timer();
	direction = 0; // Wind rose, 0 is north
	xball = 0;
	yball = 0;
	yball_prev = 0;
	xpad = 0;
	ball_timer = 0;

	// The MAX72XX is in power-saving mode on startup,
	// we have to do a wakeup call
	pinMode(POTPIN, INPUT);

	SetupMatrix();

	//lc2.shutdown(0, false);
	// Set the brightness to a medium values
	//lc2.setIntensity(0, 8);
	// and clear the display
	//lc2.clearDisplay(0);
	//ClearMatrix(); // 2 colors
	randomSeed(analogRead(0));
#ifdef DEBUG
	Serial.begin(9600);
	Serial.println("Pong");
#endif
	newGame();
	ball_timer = timer.every(BALL_DELAY, moveBall);
}


void pongLoop() {
	while (true) {
		timer.update();
		// Move pad
		setPad();
		if (shouldEndGame2) {
			//lc2.clearDisplay(0);
			ClearMatrix(); // 2 colors
			return;
		}
#ifdef DEBUG
		Serial.println(xpad);
#endif
		// Update screen
		drawGame();
		if (checkLoose()) {
			debug("LOOSE");
			gameOver();
			newGame();
		}
		delay(GAME_DELAY);
	}
}

void runPong() {
	pongSetup();
	pongLoop();
}