#include "TicTacToe.h"

bool shouldPlayTTT;
int player = 1;
int winner = 0;
int numberOfSteps = 0;
int gameBoard[3][3] = {
	{0,0,0},
	{0,0,0},
	{0,0,0}
};

void printGrid() {
	for (int i = 0; i < 8; i++) {
		SetLed(RED, 2, i, HIGH);
		SetLed(GREEN, 2, i, HIGH);
		SetLed(RED, 5, i, HIGH);
		SetLed(GREEN, 5, i, HIGH);
		SetLed(RED, i, 2, HIGH);
		SetLed(GREEN, i, 2, HIGH);
		SetLed(RED, i, 5, HIGH);
		SetLed(GREEN, i, 5, HIGH);
	}
}

void printSquare(int row, int col) {
	byte color = (player == 1 ? GREEN : RED);
	int startRow = row * 3;
	int startCol = col * 3;
	SetLed(color, startRow, startCol, HIGH);
	SetLed(color, startRow, startCol + 1, HIGH);
	SetLed(color, startRow + 1, startCol, HIGH);
	SetLed(color, startRow + 1, startCol + 1, HIGH);
}

void turnOffSquare(int row, int col) {
	int startRow = row * 3;
	int startCol = col * 3;
	SetLed(RED, startRow, startCol, LOW);
	SetLed(RED, startRow, startCol + 1, LOW);
	SetLed(RED, startRow + 1, startCol, LOW);
	SetLed(RED, startRow + 1, startCol + 1, LOW);
	SetLed(GREEN, startRow, startCol, LOW);
	SetLed(GREEN, startRow, startCol + 1, LOW);
	SetLed(GREEN, startRow + 1, startCol, LOW);
	SetLed(GREEN, startRow + 1, startCol + 1, LOW);
}

void printWinner(int r1, int c1, int r2, int c2, int r3, int c3) {
	for (int i = 0; i < 4; i++) {
		printSquare(r1, c1);
		printSquare(r2, c2);
		printSquare(r3, c3);
		delay(600);
		turnOffSquare(r1, c1);
		turnOffSquare(r2, c2);
		turnOffSquare(r3, c3);
		delay(600);
	}
}

bool checkIfWin(int lastRow, int lastCol) {
	if (gameBoard[lastRow][0] == gameBoard[lastRow][1] && gameBoard[lastRow][1] == gameBoard[lastRow][2]) {
		printWinner(lastRow, 0, lastRow, 1, lastRow, 2);
		return true;
	}
	if (gameBoard[0][lastCol] == gameBoard[1][lastCol] && gameBoard[1][lastCol] == gameBoard[2][lastCol]) {
		printWinner(0, lastCol, 1, lastCol, 2, lastCol);
		return true;
	}
	if (gameBoard[0][0] == player && gameBoard[0][0] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][2]) {
		printWinner(0, 0, 1, 1, 2, 2);
		return true;
	}
	if (gameBoard[0][2] == player && gameBoard[0][2] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][0]) {
		printWinner(0, 2, 1, 1, 2, 0);
		return true;
	}
	return false;
}

void ticTacToeSetup() {
	player = 1;
	winner = 0;
	numberOfSteps = 0;
	shouldPlayTTT = true;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			gameBoard[i][j] = 0;
		}
	}
	SetupMatrix();
	printGrid();
}

void readDataTTT() {
	if (Serial.available()) {
		String data = Serial.readString();
		switch (data[0]) {
		case ('b') :
			shouldPlayTTT = false;
			return;
		default:
			int rowNum = data[0] - '0';
			rowNum = 2 - rowNum;
			int colNum = data[1] - '0';
			gameBoard[rowNum][colNum] = player;
			printSquare(rowNum, colNum);
			if (checkIfWin(rowNum, colNum) || (++numberOfSteps == 9)) {
				runTicTacToe();
			}
			player = (player == 1 ? 2 : 1);
		}
	}
}

void ticTacToeLoop() {
	while (shouldPlayTTT) {
		readDataTTT();
	}
	ClearMatrix();
}

void runTicTacToe() {
	ticTacToeSetup();
	ticTacToeLoop();
}