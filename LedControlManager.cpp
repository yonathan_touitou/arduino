#include "LedControlManager.h"

LedControl lc = LedControl(12, 11, 10, 2);

////////////////////////////////////////////// Functions to support common catode //////////////////////////////////////////////////////////
int maxInShutdown = GREEN;                // tells which MAX7221 is currently off
unsigned long ISRTime;
///////////////////////////////ISR Timer Functions ///////////////////////////
ISR(TIMER2_COMPA_vect) {  //This ISR toggles shutdown between the 2MAX7221's
	if (maxInShutdown == RED) {
		lc.shutdown(GREEN, true);  // The order here is critical - Shutdown first!
		lc.shutdown(RED, false);   // . . . Then restart the other.
		maxInShutdown = GREEN;
	}
	else {
		lc.shutdown(RED, true);
		lc.shutdown(GREEN, false);
		maxInShutdown = RED;
	}
}

void setISRtimer() {  // setup ISR timer controling toggleing
	TCCR2A = 0x02;                        // WGM22=0 + WGM21=1 + WGM20=0 = Mode2 (CTC)
	TCCR2B = 0x05;                // CS22=1 + CS21=0 + CS20=1 = /128 prescaler (125kHz)
	TCNT2 = 0;                            // clear counter
	OCR2A = ISR_FREQ;                     // set TOP (divisor) - see #define
}

void startISR() {  // Starts the ISR
	TCNT2 = 0;                            // clear counter (needed here also)
	TIMSK2 |= (1 << OCIE2A);                  // set interrupts=enabled (calls ISR(TIMER2_COMPA_vect)
}

void stopISR() {    // Stops the ISR
	TIMSK2 &= ~(1 << OCIE2A);                  // disable interrupts
}

/////////   Wrappers for LedControl functions . . . //////////
void SetLed(byte Color, byte Row, byte Col, byte State) {
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.setLed(Color, Col, Row, State);
	startISR();           // enable interrupts again
}

void SetRow(byte Color, byte Row, byte State) {
	SetColumn(Color, Row, State); return;
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.setRow(Color, Row, State);
	startISR();           // enable interrupts again
}

void SetColumn(byte Color, byte Col, byte State) {
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.setColumn(Color, Col, State);
	startISR();           // enable interrupts again
}

void ClearMatrix() {
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.clearDisplay(GREEN);
	lc.clearDisplay(RED);
	startISR();           // enable interrupts again
}

void ClearGREEN() {
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.clearDisplay(GREEN);
	startISR();           // enable interrupts again
}

void ClearRED() {
	stopISR();            // disable interrupts - stop toggling shutdown when updating
	lc.clearDisplay(RED);
	startISR();           // enable interrupts again
}

void SetupMatrix() {
	lc.shutdown(RED, false);
	lc.setIntensity(RED, 15); // Set the brightness to a medium values
	lc.clearDisplay(RED);
	lc.shutdown(GREEN, false);
	lc.setIntensity(GREEN, 6); // Set the brightness to a medium values
	lc.clearDisplay(GREEN);
}

void printChar(byte color, char c) {
	lc.setChar(GREEN, 0, 'a', false);
	delay(1000);
	ClearMatrix();
	lc.setChar(GREEN, 0, 'a', true);
	delay(1000);
	ClearMatrix();
	lc.setChar(GREEN, 1, 'a', false);
	delay(1000);
	ClearMatrix();
	lc.setChar(GREEN, 1, 'a', true);
	ClearMatrix();
}

////////////////////////////////////////////////////////////////////////////////