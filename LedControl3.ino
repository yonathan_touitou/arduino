#include "LedControl3.h"

void setup(){
	Serial.begin(9600);
	runMoodCoffee();
}

void loop() {
	if (Serial.available()) {
		char selection = Serial.read();
			switch (selection) {
			case (SELECTION_SNAKE) :
				runSnake();
				break;
			case (SELECTION_PONG) :
				runPong();
				break;
			case (SELECTION_DRAW) :
				runDraw();
				break;
			case (SELECTION_TICTACTOE) :
				runTicTacToe();
				break;
			
			case (SELECTION_MOODCOFFEE) :
				runMoodCoffee();
				break;
			case (SELECTION_MOODLOVE) :
				runMoodLove();
				break;
			case (SELECTION_MOODSMILY) :
				runMoodSmily();
				break;
			case (SELECTION_MOODSTARS) :
				runMoodStars();
				break;
			case (SELECTION_MOODPARTY1) :
				runMoodParty1();
				break;
			case (SELECTION_MOODPARTY2) :
				runMoodParty2();
				break;
			case (SELECTION_MOODPARTY3) :
				runMoodParty3();
				break;

			case (SELECTION_TESTS) :
				runTests();
				break;
		}
	}
}
