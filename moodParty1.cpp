#include "moodParty1.h"
#include "moodCommon.h"


void moodParty1() {
	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arr_square0[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arr_square1[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arr_square2[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arr_square3[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arr_square2[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arr_square1[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();
}


void moodParty1Setup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodParty1Loop() {
	while (moodShouldPlay) {
		moodParty1();
	}
	ClearMatrix();
}

void runMoodParty1() {
	moodParty1Setup();
	moodParty1Loop();
}