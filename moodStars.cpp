#include "moodStars.h"
#include "moodCommon.h"


void moodStars() {
	for (int i = 0; i < 9; i++) {
		for (int r = 0; r < 8; r++) {
			SetRow(GREEN, r, arr_stars[i][r]);
			SetRow(RED, r, arr_stars[i][r]);
		}
		if (checkIfMoodSouldEnd()) { return; }
		delay(400);
		if (checkIfMoodSouldEnd()) { return; }
	}
}


void moodStarsSetup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodStarsLoop() {
	while (moodShouldPlay) {
		moodStars();
	}
	ClearMatrix();
}

void runMoodStars() {
	moodStarsSetup();
	moodStarsLoop();
}