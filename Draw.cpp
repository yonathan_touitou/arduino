#include "Draw.h"

byte selectedColor = COLOR_NONE;
bool draw_shouldPlay = true;

void drawSetup() {
	draw_shouldPlay = true;
	SetupMatrix();
}

void shutdownLed(int row, int col) {
	SetLed(RED, row, col, LOW);
	SetLed(GREEN, row, col, LOW);
	//SetLed(BLUE, row, col, LOW);
}

void readData() {
	if (Serial.available()) {
		String data = Serial.readString();
		switch (data[0]) {
			case ('b'):
				draw_shouldPlay = false;
				return;
			case ('c') :
				ClearMatrix();
				return;
			case(COLOR_BLUE) :
				// Not yet implemented
			case(COLOR_GREEN) :
				selectedColor = GREEN;
				return;
			case(COLOR_RED) :
				selectedColor = RED;
				return;
			case (COLOR_YELLOW) :
				selectedColor = YELLOW;
				return;
			case(COLOR_NONE) :
				selectedColor = NONE;
				return;
			default:
				int rowNum = data[0] - '0';
				rowNum = 7 - rowNum;
				int colNum = data[1] - '0';
				shutdownLed(rowNum, colNum);
				if (selectedColor == YELLOW) {
					SetLed(RED, rowNum, colNum, HIGH);
					SetLed(GREEN, rowNum, colNum, HIGH);
				}
				else if (selectedColor != NONE) {
					SetLed(selectedColor, rowNum, colNum, HIGH);
				}
		}
	}
}

void drawLoop() {
	printChar(GREEN, 'a');
	while (draw_shouldPlay) {
		readData();
	}
	ClearMatrix();
}

void runDraw() {
	drawSetup();
	drawLoop();
}