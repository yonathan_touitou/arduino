#include "ReadIrManager.h"

bool readIR(int direction) {
	int IRemitter = IR_LEDS[direction][EMITTER];
	int IRpin = IR_LEDS[direction][PIN];
	int ambientIR, obstacleIR, value[NUM_OF_READS], distanceSum = 0;
	pinMode(IRemitter, OUTPUT);
	digitalWrite(IRemitter, LOW);

	for (int i = 0; i < NUM_OF_READS; i++) {
		digitalWrite(IRemitter, LOW);
		delay(1);
		ambientIR = analogRead(IRpin);
		digitalWrite(IRemitter, HIGH);
		delay(1);
		obstacleIR = analogRead(IRpin);
		value[i] = ambientIR - obstacleIR;
		distanceSum += value[i];
	}

	int distance = distanceSum / NUM_OF_READS;
	return (distance > MIN_DISTANCE);
}

char getIrInput() {
	Serial.begin(9600);
	if (readIR(RIGHT_INDEX)) {
		return RIGHT;
	}
	if (readIR(LEFT_INDEX)) {
		return LEFT;
	}
	if (readIR(UP_INDEX)) {
		return UP;
	}
	if (readIR(DOWN_INDEX)) {
		return DOWN;
	}
	return NO_IR_INPUT;
}