#include "moodLove.h"
#include "moodCommon.h"

void moodLove() {
	for (int r = 1; r < 8; r++) {
		SetRow(RED, r, arr_love[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearRED();

	for (int r = 1; r < 8; r++) {
		SetRow(GREEN, r, arr_love[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearGREEN();
}

void moodLoveSetup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodLoveLoop() {
	while (moodShouldPlay) {
		moodLove();
	}
	ClearMatrix();
}

void runMoodLove() {
	moodLoveSetup();
	moodLoveLoop();
}