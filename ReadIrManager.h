#ifndef ReadIrManager_h
#define ReadIrManager_h
#include "LedControl.h"

const char UP = 'w';
const char DOWN = 's';
const char RIGHT = 'd';
const char LEFT = 'a';
const char NO_IR_INPUT = '0';

const int NUM_OF_READS = 5;
const int MIN_DISTANCE = 370;

const int UP_INDEX = 0;
const int DOWN_INDEX = 1;
const int RIGHT_INDEX = 2;
const int LEFT_INDEX = 3;
const int PIN = 0;
const int EMITTER = 1;

//{analog, digital}
const int IR_LEDS[4][2] = {
	{0,2},
	{1,3},
	{2,4},
	{3,5},
	};

char getIrInput();

#endif	//ReadIrManager.h