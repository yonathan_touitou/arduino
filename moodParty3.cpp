#include "moodParty3.h"
#include "moodCommon.h"


void moodParty3() {
	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arrr_square0[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arrr_square1[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arrr_square2[r] | arrr_square0[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arrr_square3[r] | arrr_square1[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();

	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arrr_square3[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arrr_square2[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, arrr_square1[r] | arrr_square3[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }

	for (int r = 0; r < 8; r++) {
		SetRow(RED, r, arrr_square0[r] | arrr_square2[r]);
	}
	if (checkIfMoodSouldEnd()) { return; }
	delay(400);
	if (checkIfMoodSouldEnd()) { return; }
	ClearMatrix();
}

void moodParty3Setup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodParty3Loop() {
	while (moodShouldPlay) {
		moodParty3();
	}
	ClearMatrix();
}

void runMoodParty3() {
	moodParty3Setup();
	moodParty3Loop();
}