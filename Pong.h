#include "LedControl3.h"
#include "Timer.h"


#define POTPIN A5 // Potentiometer
#define PADSIZE 3
#define BALL_DELAY 150
#define GAME_DELAY 5
#define BOUNCE_VERTICAL 1
#define BOUNCE_HORIZONTAL -1
#define NEW_GAME_ANIMATION_SPEED 50
#define HIT_NONE 0
#define HIT_CENTER 1
#define HIT_LEFT 2
#define HIT_RIGHT 3