#ifndef LedControlManager_h
#define LedControlManager_h
#include "LedControl.h"

#define ISR_FREQ 30     //190=650Hz    // Sets the speed of the ISR - LOWER IS FASTER
#define NONE -1
#define GREEN 0                         // The address of the MAX7221 for the green leds
#define RED 1                           // The address of the MAX7221 for the red leds
#define YELLOW 2

void SetLed(byte Color, byte Row, byte Col, byte State);

void SetRow(byte Color, byte Row, byte State);

void SetColumn(byte Color, byte Col, byte State);

void ClearMatrix();

void ClearGREEN();

void ClearRED();

void SetupMatrix();

void printChar(byte color, char c);

#endif	//LedControlManager.h