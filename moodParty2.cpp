#include "moodParty2.h"
#include "moodCommon.h"


void moodParty2() {
	for (int i = 0; i < 8; i++) {
		for (int r = 0; r < 8; r++) {
			SetRow(GREEN, r, arr_lines[i][r]);
			SetRow(RED, r, arr_lines[i][7 - r]);
		}
		if (checkIfMoodSouldEnd()) { return; }
		delay(180);
		if (checkIfMoodSouldEnd()) { return; }
		ClearMatrix();

	}
}

void moodParty2Setup() {
	moodShouldPlay = true;
	SetupMatrix();
}

void moodParty2Loop() {
	while (moodShouldPlay) {
		moodParty2();
	}
	ClearMatrix();
}

void runMoodParty2() {
	moodParty2Setup();
	moodParty2Loop();
}