#include "Tests.h"

void testsSetup() {
	SetupMatrix();
}

void testsLoop() {
	for (int r = 0; r < 8; r++) {
		SetRow(GREEN, r, turnOnAll[r]);
		SetRow(RED, r, turnOnAll[r]);
	}
	while (true) {
		if (Serial.available()) {
			String data = Serial.readString();
			if (data[0] == 'b') {
				ClearMatrix();
				return;
			}
		}
	}
}

void runTests() {
	testsSetup();
	testsLoop();
}